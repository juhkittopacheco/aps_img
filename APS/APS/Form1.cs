﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Seleciona a imagem que será verificada
        private void btnEscolherImagem_Click(object sender, EventArgs e)
        {
            //tratamento de erro no sitema
            try
            {
                //escolhe uma image
                pictureBox1.Image = null;
                DialogResult IsFileChosen = openFileDialog1.ShowDialog();
                if (IsFileChosen == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDialog1.ValidateNames == true)
                    {
                        //faz com que a imagem apareceça no canto esquerdo
                        pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void btnVerificar_Click(object sender, EventArgs e)
        {
            try
            {
                //seleciona as cores que identificam fogo em meio a uma floresta
                Boolean IsColorFound = false;
                Color[] fogo = {
                    Color.FromArgb(255, 93, 82),
                    Color.FromArgb(249, 179, 84),
                    Color.FromArgb(252, 243, 28),
                    Color.FromArgb(222, 173, 52),
                    Color.FromArgb(253, 123, 48),
                    Color.FromArgb(240, 144, 64),
                    Color.FromArgb(252, 88, 84),
                    Color.FromArgb(246, 130, 0),
                    Color.FromArgb(253, 86, 85),
                    Color.FromArgb(237, 140, 47),
                    Color.FromArgb(242, 94, 94)
                };

                if (pictureBox1.Image != null)
                {
                    //Transforma a imagem em um sistema de BitMap
                    Bitmap bmp = new Bitmap(pictureBox1.Image);

                    //Percorre todo o BitMap para encontrar se algum sinal da cores que foram prévimente seleciondas
                    for (int i = 0; i < pictureBox1.Image.Height; i++)
                    {
                        for (int j = 0; j < pictureBox1.Image.Width; j++)
                        {
                            //pega as cores de cada pixel
                            Color now_color = bmp.GetPixel(j, i);

                            for (int o = 0; o < fogo.Length; o++)
                            {
                                //compara os pixels da imagem com cada cor referente a fogo que poça haver na imagem
                                if (now_color.ToArgb() == fogo[o].ToArgb())
                                {
                                    IsColorFound = true;
                                    MessageBox.Show("Sinais de fogo encontrado, avisar a central.");
                                    break;
                                }
                            }
                            if (IsColorFound == true)
                            {
                                break;
                            }

                        }
                        if (IsColorFound == true)
                        {
                            break;
                        }
                    }

                    if (IsColorFound == false)
                    {
                        MessageBox.Show("Nenhum sinal de fogo encontrado.");
                    }
                }
                else
                {
                    MessageBox.Show("Selecione uma imagem");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
    }
}
